# Web Stats
## Get started
### Build instructions
Requirements : 
- node
- npm

Install the dependencies…
```bash
npm install
```

…then build the app
```bash
npm run build
```

The extension code is located in the `public` directory

### Development instructions
Install the dependencies…
```bash
npm install
```

…then start [Rollup](https://rollupjs.org):
```bash
npm run dev
```

## Credits
This extension isn't licensed (yet?).

### Library & Technologies
- Svelte released under MIT License
- Rollup released under MIT License
- svelte-frappe-charts by Dave Lunny released under MIT license
- Frappe Charts released under MIT License

Code from Carbonalyzer (released under MIT license) was used.

### Icon
The icon of the addon is "timer" by Iconika under CC-By
