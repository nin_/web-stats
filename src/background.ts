import { extractHostname } from "./lib/urls";
import { stats } from "./lib/stats";

const isChrome = () => {
    // @ts-ignore
    return typeof browser === "undefined";
};

const getOrigin = (requestDetails) => {
    if (isChrome()) {
        return extractHostname(
          !requestDetails.initiator
            ? requestDetails.url
            : requestDetails.initiator
        );
    }
    return extractHostname(!requestDetails.documentUrl ? requestDetails.url : requestDetails.documentUrl);
};

/**
 * TODO:
 * Check if event.data.byteLength from webRequest.StreamFilter.ondata
 * is more accurate and cover more (only on FF with webRequestBlocking)
 **/
const listener = requestDetails => {
    const origin = getOrigin(requestDetails);
    const contentLength = requestDetails.responseHeaders.find(
      element => element.name.toLowerCase() === "content-length"
    );

    const requestSize = contentLength ? parseInt(contentLength.value, 10) : 0;

    stats.addBytes(origin, requestSize);

    return {};
};

// @ts-ignore
chrome.webRequest.onHeadersReceived.addListener(listener,
  { urls: ["<all_urls>"] },
  ["responseHeaders"]
);

