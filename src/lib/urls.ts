export const extractHostname = (url: string): string =>
    (url.indexOf("//") > -1 ? url.split("/")[2] : url.split("/")[0])
        .split(":")[0]
        .split("?")[0]
        .replace("www.", '');
