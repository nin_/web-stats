import type { Readable } from "svelte/store";
import { createStorage } from "./storage";

export type Stats = { [origin: string]: number }
type StatsStore = Readable<Stats> & {
    reset: () => void,
    addBytes: (origin: string, byte_length: number) => void
}

const createStatsStore = (): StatsStore => {
    const storage = createStorage("stats", {});
    const { update, subscribe } = storage;

    const addBytes = (origin: string, byte_length: number) => {
        update((stats: Stats) => {
            stats[origin] = ~~stats[origin] + byte_length;
            return stats;
        });
    };

    const reset = () => {
        update(() => {})
    }

    return { reset, subscribe, addBytes };
};

export const stats = createStatsStore();
