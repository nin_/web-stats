import type { Updater, Writable } from "svelte/store";

export type Storage<T> = Writable<T> & { reset: () => void }
export const createStorage = (key: string, initialValue: any = undefined): Storage<any> => {
    let value = typeof initialValue === "function" ? initialValue() : initialValue;

    let subs = []; // subscriber's handlers

    const subscribe = (handler) => {
        subs = [...subs, handler]; // add handler to the array of subscribers
        handler(value); // call handler with current value
        return () => (subs = subs.filter((sub) => sub !== handler)); // return unsubscribe function
    };

    const set = (new_value) => {
        value = new_value; // update value

        subs.forEach((sub) => sub(value)); // update subscribers
        // @ts-ignore
        chrome.runtime.sendMessage({
            action: "storeUpdate",
            key,
            value,
        });
    };

    const update = (callback: Updater<any>) => set(callback(value));

    // @ts-ignore
    chrome.runtime.onMessage.addListener((request) => {
        if ("storeUpdate" !== request.action || request.key !== key) return true;
        subs.forEach((sub) => sub(request.value)); // update subscribers
        return true;
    });

    return {
        set,
        update,
        reset: () => {
            set(initialValue);
        },
        subscribe,
    };
};
