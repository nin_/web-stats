export default (translationKey, data = []) => {
  return chrome.i18n.getMessage(translationKey, data);
};
